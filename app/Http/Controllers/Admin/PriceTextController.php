<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PriceText;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class PriceTextController extends Controller
{
    public function index(){


        $price=PriceText::first();
        return view('admin.price_text.index',compact('price'));
    }

    public function update(Request $request,$id){
        $price_text=PriceText::first();


        $price_text->update([
            'ar'=>['title'    => $request->title_ar,
                'description' => $request->description_ar
            ],
            'en'=>['title'    => $request->title_en,
                'description' => $request->description_en
            ]
        ]);

        return redirect()->back();
    }

}
