@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active"> {{trans('sidebar.settings')}} </li>

                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="row layout-spacing " >

            <!-- Content -->
            <div class="col-12" style="margin:2% 2% auto;">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <div class=" " style="padding:2% 2% 0px; " >
                            <h3 class=""> {{trans('sidebar.settings')}} </h3>
                        </div>

                        <div class="" style="padding: 2%;">
                            <div class="container">

                                <form class=" g-3" method="post" action="{{route('admin_panel.metas.update',1)}}" enctype="multipart/form-data" >
                                    @method('PATCH')
                                    @csrf
                                    @if ($errors->any())

                                        <div class="alert alert-danger">

                                            <ul style="list-style: none;margin:0">

                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach

                                            </ul>

                                        </div>

                                    @endif

                                    <div class="col-md-6">
                                        <label for="inputAddress" class="form-label">og image </label>
                                        <img alt="avatar" src="/{{$meta->og_image}}"  width="200" height="200"/>
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="og_image" >
                                        <p style="color: red"> {{trans('admin.image_size')}}  55*55</p>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputAddress" class="form-label">{{trans('admin.icon')}}</label>
                                        <img alt="avatar" src="/{{$meta->icon}}"  width="200" height="200"/>
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="icon" >
                                        <p style="color: red"> {{trans('admin.image_size')}}  55*55</p>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputAddress" class="form-label">{{trans('admin.header_logo')}} </label>
                                        <img alt="avatar" src="/{{$meta->header_logo}}"  width="200" height="200"/>
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="header_logo" >
                                        <p style="color: red"> {{trans('admin.image_size')}}  55*55</p>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputAddress" class="form-label">{{trans('admin.footer_logo')}} </label>
                                        <img alt="avatar" src="/{{$meta->footer_logo}}"  width="200" height="200"/>
                                        <br><br>
                                        <input type="file" class="form-control"  placeholder="1234 Main St" name="footer_logo" >
                                        <p style="color: red"> {{trans('admin.image_size')}}  55*55</p>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.web_color')}} </label>
                                        <input type="text" class="form-control"  name="web_color" value="{{$meta->web_color}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.title_ar')}}</label>
                                        <input type="text" class="form-control"  name="title_ar" value="{{$meta->translate('ar')->title}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.des_ar')}} </label>
                                        <textarea  class="form-control"  name="description_ar" >{{$meta->translate('ar')->description}}</textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.title_en')}} </label>
                                        <input type="text" class="form-control"  name="title_en" value="{{$meta->translate('en')->title}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.des_en')}} </label>
                                        <textarea class="form-control"  name="description_en" >{{$meta->translate('en')->description}}</textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.keywords_ar')}}</label>
                                        <input type="text" class="form-control"  name="keywords_ar" value="{{$meta->translate('ar')->keywords}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> {{trans('admin.keywords_en')}}</label>
                                        <input type="text" class="form-control"  name="keywords_en" value="{{$meta->translate('en')->keywords}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.og_title_en')}} </label>
                                        <input type="text" class="form-control"  name="og_title_en" value="{{$meta->translate('en')->og_title}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">{{trans('admin.og_title_ar')}}</label>
                                        <input type="text" class="form-control"  name="og_title_ar" value="{{$meta->translate('ar')->og_title}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> {{trans('admin.footer_ar')}}</label>
                                        <textarea type="text" class="form-control"  name="footer_ar" >{{$meta->translate('ar')->footer}}</textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> {{trans('admin.footer_en')}}</label>
                                        <textarea type="text" class="form-control"  name="footer_en" >{{$meta->translate('en')->footer}}</textarea>
                                    </div>

                                    <div class="col-6">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary">{{trans('admin.edit')}}</button>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection
